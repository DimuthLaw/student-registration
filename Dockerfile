FROM openjdk:11.0.16
COPY target/ABCInstitute-0.0.1-SNAPSHOT.jar ABCInstitute.jar
ENTRYPOINT ["java","-jar","/ABCInstitute.jar"]