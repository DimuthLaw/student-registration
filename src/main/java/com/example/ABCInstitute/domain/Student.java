package com.example.ABCInstitute.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Table(name = "student")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Student
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column("stud_id")
    private Long studId;

    @Column("stud_name")
    private String studentName;

    @Column("stud_address")
    private String studentAddress;

    @Column("phone_no")
    private Long phoneNumber;

}
