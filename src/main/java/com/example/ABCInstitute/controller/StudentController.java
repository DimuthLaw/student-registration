package com.example.ABCInstitute.controller;

import com.example.ABCInstitute.domain.Student;
import com.example.ABCInstitute.dto.ApiResponse;
import com.example.ABCInstitute.dto.StudentInfoDto;
import com.example.ABCInstitute.service.StudentService;
import com.example.ABCInstitute.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/student")
@CrossOrigin
public class StudentController
{
    @Autowired
    private StudentService studentService;

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentController.class);

    /**
     * Creates a new student record in the database.
     * @param studentInfoDto The DTO containing the details of the student to be created.
     * @return A Mono that return HTTP response containing a success message if the operation succeeds.
     */
    @PostMapping()
    public Mono<ResponseEntity<ApiResponse>> createStudent(@RequestBody StudentInfoDto studentInfoDto)
    {
        // Record the start time
        long startTime = System.currentTimeMillis();

        LOGGER.info("saveRequest : request={}", CommonUtil.convertToString(studentInfoDto));

        // Convert the DTO to a domain object
        Student student = new Student();
        BeanUtils.copyProperties(studentInfoDto, student);

        // Call the student service
        return studentService.createStudent(student)
                .map(x->{
                    LOGGER.info("saveResponse : timeTaken={}|response={}",
                            CommonUtil.getTimeTaken(startTime),
                            CommonUtil.convertToString(x));

                    // Return an HTTP response with a success message
                    return new ResponseEntity<>(
                            new ApiResponse(200, "You’ve created the student account successfully."), HttpStatus.OK);
                });
    }
}
