package com.example.ABCInstitute.exception;

import lombok.Getter;

@Getter
public enum ErrorCode {

    NO_DATA_FOUND(404,"No data found.");

    private int code;
    private String description;


    ErrorCode(int code, String description)
    {
        this.code = code;
        this.description = description;
    }

}
