package com.example.ABCInstitute.exception;


public class StudentException extends Exception
{

    private static final long serialVersionUID = 1L;
    private final ErrorCode error;
    private final String message;
    private final Object data;

    public StudentException(ErrorCode exceptionError) {
        this.error = exceptionError;
        this.message = exceptionError.getDescription();
        this.data=null;
    }

    public StudentException(ErrorCode error, String errorMessage) {
        this.error = error;
        this.message = errorMessage;
        this.data=null;
    }

    public ErrorCode getError() {
        return error;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }

}
