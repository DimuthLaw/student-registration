package com.example.ABCInstitute.service.Impl;

import com.example.ABCInstitute.domain.Student;
import com.example.ABCInstitute.exception.ErrorCode;
import com.example.ABCInstitute.exception.StudentException;
import com.example.ABCInstitute.repository.StudentRepository;
import com.example.ABCInstitute.service.StudentService;
import com.example.ABCInstitute.util.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class StudentServiceImpl implements StudentService
{

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Autowired
    private StudentRepository studentRepository;


    /**
     * Creates a new student record in the database.
     * @param student The Student object containing the details of the student to be created.
     * @return A Mono that returns the newly created student record if the operation succeeds.
     */
    @Override
    public Mono<Student> createStudent(Student student)
    {
        // Record the start time
        long startTime = System.currentTimeMillis();

        LOGGER.info("createStudentRequest: request={}", CommonUtil.convertToString(student));

        // Save the student
        return studentRepository.save(student)
                .map(response -> {
                    LOGGER.info("createStudentResponse : timeTaken={}|id={}|response={}", CommonUtil.getTimeTaken(startTime), response, CommonUtil.convertToString(response));
                    // Return the newly created student record
                    return response;

                    // If the operation fails, return an error with a StudentException containing the error code
                }).switchIfEmpty(Mono.error(new StudentException(ErrorCode.NO_DATA_FOUND)));
    }
}