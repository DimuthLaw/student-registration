package com.example.ABCInstitute.service;

import com.example.ABCInstitute.domain.Student;
import reactor.core.publisher.Mono;

public interface StudentService
{
    Mono<Student>createStudent(Student student);

}
