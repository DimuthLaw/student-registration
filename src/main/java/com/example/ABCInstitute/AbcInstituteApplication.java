package com.example.ABCInstitute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbcInstituteApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbcInstituteApplication.class, args);
	}

}
