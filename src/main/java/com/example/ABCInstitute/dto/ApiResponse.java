package com.example.ABCInstitute.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ApiResponse
{

    @ApiModelProperty(notes = "Response status code")
    private int statusCode;

    @ApiModelProperty(notes = "Response message")
    private String message;

    @JsonInclude(content = JsonInclude.Include.ALWAYS)
    @ApiModelProperty(notes = "Response data")
    private Object data;

    public ApiResponse()
    {
        super();
    }

    public ApiResponse(int statusCode, String message)
    {
        super();
        this.statusCode = statusCode;
        this.message = message;
    }

    public ApiResponse(int statusCode, String message, Object data)
    {
        super();
        this.statusCode = statusCode;
        this.message = message;
        this.data = data;
    }

}
