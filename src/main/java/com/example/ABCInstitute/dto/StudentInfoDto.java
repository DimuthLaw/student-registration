package com.example.ABCInstitute.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentInfoDto
{

    private Long studId;
    private String studentName;
    private String studentAddress;
    private Long phoneNumber;

}
