pipeline {

    agent any

    tools {
        maven "MAVEN"
    }

    environment {
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "10.10.93.57:8081"
        NEXUS_REPOSITORY = "Ooredoo-backend"
        NEXUS_CREDENTIAL_ID = "nexus-credentials"
        ARTIFACT_VERSION = "${BUILD_NUMBER}"
        DOCKER_REGISTRY_URL = "http://10.10.93.57:8081/repository/ooredoo-docker-hub"
        DOCKER_IMAGE_NAME = "studreg"
        DOCKER_IMAGE_TAG = "${BUILD_NUMBER}"
    }

    stages {

        stage("Maven Build") {
            steps {
                script {
                   sh "mvn clean package"
                }
            }
        }

        stage("Publish to Nexus Repository") {
            steps {
                script {
                    pom = readMavenPom file: "pom.xml";
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    artifactPath = filesByGlob[0].path;
                    artifactExists = fileExists artifactPath;

                    if (artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";

                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            ]
                        );
                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
        }

        stage("Push Docker Image") {
            steps {
                script {
                    def dockerImageTag = "${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}"
                    def dockerImage = docker.build(dockerImageTag, "--build-arg VERSION=${BUILD_NUMBER} .")
                    docker.withRegistry("${DOCKER_REGISTRY_URL}")
                    {
                        dockerImage.push()
                    }
                }
            }
        }
    }
}


